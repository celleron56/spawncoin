minetest.register_craftitem("spawncoin:token", {
	description = "use this token in the shop directly next to the spawn-point to get a free starter-box",
	inventory_image = "spawncoin_token.png"
})
minetest.register_craftitem("spawncoin:ftoken", {
	description = "a token with the inscription use this token in the shop directly next to the spawn-point to get a free starter-box ",
	inventory_image = "spawncoin_token.png"
})
minetest.register_craft({
	type = "cooking",
	output = "spawncoin:ftoken",
	recipe = "default:coalblock",
	cooktime = 100,
})